package pl.edu.wat.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OutputDto {
    private String label;
    private Float probability;
}
