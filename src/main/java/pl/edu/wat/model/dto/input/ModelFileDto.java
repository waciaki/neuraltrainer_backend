package pl.edu.wat.model.dto.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelFileDto {
    private byte[] bytes;

    public byte[] getBytes() {
        return bytes.clone();
    }
}
