package pl.edu.wat.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

interface ModelRepo extends JpaRepository<Model, Long> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE models SET accepted = :accepted WHERE id = :modelId", nativeQuery = true)
    void updateAcceptedModel(@Param("modelId") long modelId, @Param("accepted") boolean accepted);

    List<Model> findAllByAcceptedIsTrue();
    List<Model> findAllByAcceptedIsNull();
}
