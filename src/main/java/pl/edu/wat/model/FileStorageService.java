package pl.edu.wat.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@Slf4j
class FileStorageService {
    public final Path fileStorageLocation;

    public FileStorageService() {
        this.fileStorageLocation = Paths.get("modelsdir/")
                .toAbsolutePath().normalize();

        try {
            log.info("Creating dirs");
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            log.error("Could not create the directory where the uploaded files will be stored." + ex.toString());
            ex.printStackTrace();
        }
    }

    public File storeFile(MultipartFile file, String fileName) {
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return targetLocation.toFile();

        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Optional<Resource> loadModelFileAsResource(Long modelId) {
        try {
            Path filePath = fileStorageLocation.resolve(String.valueOf(modelId) + ".zip").normalize();
            Resource resource = new UrlResource(filePath.toUri());

            if(resource.exists())
                return Optional.of(resource);
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File #" + modelId + " not found");
        }

        return Optional.empty();
    }
}