package pl.edu.wat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.edu.wat.security.dto.RegisterUserDto;
import pl.edu.wat.security.dto.UserDto;
import pl.edu.wat.security.exceptions.RegistrationException;

import java.util.Date;

@Component
public class UserFacade {
    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;

    @Autowired
    UserFacade(UserRepo userRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    public void register(RegisterUserDto registerUserDto) {
        if(userRepo.findByUsername(registerUserDto.getUsername()) != null)
            throw new RegistrationException("A user with the given name already exists");

        User user = User.builder()
                .username(registerUserDto.getUsername())
                .password(passwordEncoder.encode(registerUserDto.getPassword()))
                .email(registerUserDto.getEmail())
                .createdDate(new Date())
                .isAdmin(false)
                .build();

        userRepo.save(user);
    }

    public UserDto getCurrentlyLoggedInUser(String name) {
        if(name == null)
            return null;

        User user = userRepo.findByUsername(name);

        if(user == null)
            return null;

        return UserDto.builder()
                .name(name)
                .admin(user.getIsAdmin())
                .build();
    }
}
