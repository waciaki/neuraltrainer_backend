package pl.edu.wat.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.edu.wat.imageprocessor.ProcessedImage;
import pl.edu.wat.model.dto.ModelDto;
import pl.edu.wat.model.dto.ModelInfoDto;
import pl.edu.wat.model.dto.ModelOutputsResponse;
import pl.edu.wat.model.dto.OutputDto;
import pl.edu.wat.model.dto.input.ModelInputDto;
import pl.edu.wat.model.exceptions.NotAdminException;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
class ModelController {

    @Autowired
    private ModelFacade facade;

    private FileStorageService fileStorageService = new FileStorageService();

    @GetMapping("/models")
    List<ModelDto> getModels() {
        return facade.getModels();
    }

    @PostMapping("/model/add")
    void addModel(@RequestBody ModelInputDto input, Principal principal, HttpSession session) {
        String ownerName = principal == null ? null : principal.getName();
        facade.addModel(input, ownerName, session.getId());
    }

    @PostMapping("/model/get/outputs")
    ModelOutputsResponse getModelOutputsAmount(@RequestParam("file") MultipartFile file, HttpSession session) {
        return facade.getNumberOfOutputs(file, session.getId());
    }

    @GetMapping("/model/info/{id}")
    ModelInfoDto getModelInfo(@PathVariable Long id) {
        return facade.getModelInfo(id);
    }

    @PostMapping("/model/process")
    List<OutputDto> process(@RequestParam("file") MultipartFile file, @RequestParam("modelId") Long modelId, HttpSession session) {
        ModelInfoDto modelInfoDto = facade.getModelInfo(modelId);
        Optional<ProcessedImage> processedImage = ProcessedImage.of(file, modelInfoDto.getWidth(), modelInfoDto.getHeight());
        return processedImage.map(processedImage1 -> facade.process(processedImage1, modelId, session.getId())).orElse(new ArrayList<>());
    }

    @GetMapping("/model/download/{id}")
    @ResponseBody
    public ResponseEntity<Resource> download(@PathVariable Long id) {
        Optional<Resource> resource = fileStorageService.loadModelFileAsResource(id);

        if(resource.isPresent()) {
            String modelName = "model.zip";

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + modelName + "\"")
                    .body(resource.get());
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping("/model/accept/{id}")
    public void acceptModel(@PathVariable Long id) {
        if(!isUserAdmin())
            throw new NotAdminException();

        facade.acceptModel(id);
    }

    @PostMapping("/model/reject/{id}")
    public void rejectModel(@PathVariable Long id) {
        if(!isUserAdmin())
            throw new NotAdminException();

        facade.rejectModel(id);
    }

    @GetMapping("/models/unapproved")
    public List<ModelDto> getUnapprovedModels() {
        if(!isUserAdmin())
            throw new NotAdminException();

        return facade.getUnapprovedModels();
    }

    private boolean isUserAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }
}
