package pl.edu.wat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.wat.security.dto.RegisterUserDto;
import pl.edu.wat.security.dto.UserDto;

import java.security.Principal;

@RestController
class UserController {

    @Autowired
    private UserFacade userFacade;

    @PutMapping("/register")
    private void register(@RequestBody RegisterUserDto registerUserDto) {
        userFacade.register(registerUserDto);
    }

    @GetMapping("/me")
    private UserDto getCurrentlyLoggedInUser(Principal principal) {
        return userFacade.getCurrentlyLoggedInUser(principal == null ? null : principal.getName());
    }
}
