package pl.edu.wat.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.edu.wat.imageprocessor.ProcessedImage;
import pl.edu.wat.model.dto.ModelDto;
import pl.edu.wat.model.dto.ModelInfoDto;
import pl.edu.wat.model.dto.ModelOutputsResponse;
import pl.edu.wat.model.dto.OutputDto;
import pl.edu.wat.model.dto.input.ModelInputDto;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
class ModelFacade {
    private ModelRepo repo;
    private FileStorageService storageService;

    @Autowired
    ModelFacade(ModelRepo repo) {
        this.repo = repo;
        this.storageService = new FileStorageService();
    }

    List<ModelDto> getModels() {
        return repo.findAllByAcceptedIsTrue().stream()
                .map(this::toModelDto)
                .collect(Collectors.toList());
    }

    private ModelDto toModelDto(Model model) {
        return ModelDto.builder()
                .id(model.id)
                .name(model.name)
                .build();
    }

    void addModel(ModelInputDto input, String ownerName, String sessionId) {
        File file = storageService.fileStorageLocation.resolve(input.getIdentifier() + ".zip").toFile();
        ModelProcessor mp = ModelProcessor.process(file, sessionId);

        List<ModelLabel> labels = new ArrayList<>();

        Model model = Model.builder()
                .name(input.getName())
                .inputs(mp.getNumberOfInputs())
                .outputs(mp.getNumberOfOutputs())
                .layers(mp.getLayers().size())
                .labels(labels)
                .createdDate(new Date())
                .ownerName(ownerName)
                .accepted(null)
                .width(mp.getWidth())
                .height(mp.getHeight())
                .build();

        for(int i = 0; i < input.getOutputs().size(); ++i) {
            ModelLabel label = new ModelLabel(input.getOutputs().get(i), i);
            labels.add(label);
            label.setModel(model);
        }

        Model modelEntity = repo.save(model);
        File newName = storageService.fileStorageLocation.resolve(modelEntity.getId() + ".zip").toFile();
        boolean renamed = file.renameTo(newName);

        if(!renamed)
            log.error("Couldn't rename file: " + file + " ---> " + newName);
    }

    ModelOutputsResponse getNumberOfOutputs(MultipartFile file, String sessionId) {
        String randomString = new RandomString(12).nextString();
        File savedFile = storageService.storeFile(file, randomString + ".zip");

        ModelProcessor mp = ModelProcessor.process(savedFile, sessionId);
        return new ModelOutputsResponse(mp.getNumberOfOutputs(), randomString);
    }

    ModelInfoDto getModelInfo(Long id) {
        Model model = repo.getOne(id);
        return toModelInfoDto(model);
    }

    private ModelInfoDto toModelInfoDto(Model model) {
        model.getLabels().sort(Comparator.comparingInt(o -> o.labelOrder));
        List<String> labels = model.getLabels().stream().map(ModelLabel::getLabel).collect(Collectors.toList());

        return ModelInfoDto.builder()
                .name(model.getName())
                .inputs(model.getInputs())
                .outputs(model.getOutputs())
                .layers(model.getLayers())
                .labels(labels)
                .createdDate(model.getCreatedDate())
                .ownerName(model.getOwnerName())
                .width(model.getWidth())
                .height(model.getHeight())
                .build();
    }

    List<OutputDto> process(ProcessedImage processedImage, Long modelId, String sessionId) {
        float[] pixels = processedImage.getPixels();
        try {

            List<Float> outputs = ModelProcessor.getOutputs(pixels, modelId, sessionId);
            for(float o : outputs)
                System.out.println(o);

            Model model = repo.getOne(modelId);
            List<ModelLabel> labels = model.getLabels();
            labels.sort(Comparator.comparingInt(o -> o.labelOrder));

            List<OutputDto> outputDtoList = new ArrayList<>();
            for(int i = 0; i < outputs.size(); ++i) {
                ModelLabel label = labels.get(i);
                outputDtoList.add(new OutputDto(label.getLabel(), outputs.get(i)));
            }

            outputDtoList.sort((o1, o2) -> Math.round((o2.getProbability() - o1.getProbability()) * 1000));
            return outputDtoList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    void acceptModel(Long modelId) {
        repo.updateAcceptedModel(modelId, true);
    }

    void rejectModel(Long modelId) {
        repo.updateAcceptedModel(modelId, false);
    }

    List<ModelDto> getUnapprovedModels() {
        return repo.findAllByAcceptedIsNull().stream()
                .map(this::toModelDto)
                .collect(Collectors.toList());
    }
}
