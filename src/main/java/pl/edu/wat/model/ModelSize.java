package pl.edu.wat.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
class ModelSize {
    int width;
    int height;
}
