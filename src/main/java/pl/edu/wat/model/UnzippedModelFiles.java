package pl.edu.wat.model;

import lombok.Data;

import java.io.File;

@Data
class UnzippedModelFiles {
    private File config;
    private File h5;
    private File sizeConfig;
    //private ModelInfo info;
}
