package pl.edu.wat.model.dto.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelInputDto {
    private String name;
    private String identifier;
    private List<String> outputs;
}
