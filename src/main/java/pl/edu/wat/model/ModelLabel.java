package pl.edu.wat.model;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name="model_label")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
class ModelLabel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;

    String label;
    Integer labelOrder;

    @ManyToOne
    @JoinColumn(name="model_id", nullable=false)
    @Setter
    Model model;

    public ModelLabel(String label, Integer labelOrder) {
        this.label = label;
        this.labelOrder = labelOrder;
    }
}
