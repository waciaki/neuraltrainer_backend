package pl.edu.wat.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class Unzipper {
    private File temp;
    private File zip;

    Unzipper(String zipFilePath, String tempDirPath) {
        zip = new File(zipFilePath);
        temp = new File(tempDirPath);
    }

    UnzippedModelFiles extractFiles() throws IOException {
        deleteDirectoryRecursion(temp.toPath());
        temp.mkdir();

        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zip.getPath()));
        ZipEntry zipEntry = zis.getNextEntry();

        UnzippedModelFiles unzippedModelFiles = new UnzippedModelFiles();

        while (zipEntry != null) {
            String name = zipEntry.getName();
            File newFile = newFile(temp, zipEntry);
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }

            fos.close();

            if(name.endsWith(".h5")) {
                unzippedModelFiles.setH5(newFile);
            }
            else if(name.endsWith(".json")) {
                unzippedModelFiles.setConfig(newFile);
            }
            else if(name.endsWith(".cfg")) {
                unzippedModelFiles.setSizeConfig(newFile);
            }
            else {

            }

            zipEntry = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();

        return unzippedModelFiles;
    }

    private File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    private void deleteDirectoryRecursion(Path path) throws IOException {
        if(!path.toFile().exists())
            return;

        if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
            try (DirectoryStream<Path> entries = Files.newDirectoryStream(path)) {
                for (Path entry : entries) {
                    deleteDirectoryRecursion(entry);
                }
            }
        }

        Files.delete(path);
    }
}
