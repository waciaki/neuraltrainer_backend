package pl.edu.wat.imageprocessor;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class ProcessedImage {
    public static final int DEFAULT_WIDTH = 28;
    public static final int DEFAULT_HEIGHT = 28;

    private float[] pixels;

    private ProcessedImage(int width, int height) {
        pixels = new float[width*height];
    }

    public static Optional<ProcessedImage> of(MultipartFile file, Integer width, Integer height) {
        if(width == null) width = DEFAULT_WIDTH;
        if(height == null) height = DEFAULT_HEIGHT;

        try {
            var image = new ProcessedImage(width, height);

            InputStream in = new ByteArrayInputStream(file.getBytes());
            BufferedImage bufferedImage = ImageIO.read(in);

            ImageConverter converter = new ImageConverter();
            bufferedImage = converter.greyPixelByPixelAverage(bufferedImage);
            bufferedImage = converter.resize(bufferedImage, width, height);

            //File outputfile = new File("modelsdir", new RandomString(4).nextString() + ".png");
            //mageIO.write(bufferedImage, "png", outputfile);

            image.pixels = loadPixels(bufferedImage, width, height);

            return Optional.of(image);
        } catch (IOException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("STH REALLY WRONG");
        }

        return Optional.empty();
    }

    private static float[] loadPixels(BufferedImage image, int width, int height) {
        float[] pixels = new float[width*height];
        for(int y = 0; y < height; ++y) {
            for(int x = 0; x < width; ++x) {
                Color col = new Color(image.getRGB(x, y));
                pixels[y*width + x] = col.getRed()/255.0f;
            }
        }

        return pixels;
    }

    public float[] getPixels() {
        return pixels.clone();
    }
}
