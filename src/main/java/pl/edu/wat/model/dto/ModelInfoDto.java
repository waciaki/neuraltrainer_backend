package pl.edu.wat.model.dto;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ModelInfoDto {
    private String name;
    private Long inputs;
    private Long outputs;
    private Integer layers;
    private Date createdDate;
    private String ownerName;

    private Integer width;
    private Integer height;

    private List<String> labels;
}
