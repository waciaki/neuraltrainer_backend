package pl.edu.wat.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="models")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
class Model {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;

    String name;
    Long inputs;
    Long outputs;
    Integer layers;
    Date createdDate;

    Integer width;
    Integer height;

    String ownerName;

    Boolean accepted;

    @OneToMany(mappedBy="model", cascade = CascadeType.PERSIST)
    List<ModelLabel> labels;
}
