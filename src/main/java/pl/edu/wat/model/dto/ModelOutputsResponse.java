package pl.edu.wat.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ModelOutputsResponse {
    private long numberOfOutputs;
    private String identifier;

}
