package pl.edu.wat.imageprocessor;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;

class ImageConverter {

    BufferedImage fullConvert(BufferedImage image, int width, int height) {
        image = greyWithColorSpace(image);
        image = resize(image, width, height);

        return image;
    }

    BufferedImage greyWithColorSpace(BufferedImage image) {
        ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        ColorConvertOp op = new ColorConvertOp(cs, null);
        return op.filter(image, null);
    }

    BufferedImage resize(Image originalImage, int width, int height) {
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resized.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return resized;
    }

    BufferedImage resizeAndColorAtOnce(BufferedImage image, int width, int height) {
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        Graphics2D g = resized.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();

        return resized;
    }

    BufferedImage greyPixelByPixelAverage(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        BufferedImage grey = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        for(int y=0; y<height; y++) {
            for(int x=0; x<width; x++) {
                Color c = new Color(image.getRGB(x, y));
                //int avg = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
                int avg = Math.round(.299f*c.getRed() + .587f*c.getGreen() + .114f*c.getBlue());

                Color newColor = new Color(avg, avg, avg);
                grey.setRGB(x,y,newColor.getRGB());
            }
        }

        return grey;
    }
}
