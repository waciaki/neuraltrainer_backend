package pl.edu.wat.model.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ModelDto {
    private Long id;
    private String name;

}
