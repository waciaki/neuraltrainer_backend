package pl.edu.wat.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.layers.FeedForwardLayer;
import org.deeplearning4j.nn.layers.BaseLayer;
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.data.util.Pair;
import pl.edu.wat.imageprocessor.ProcessedImage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@Getter
@Slf4j
class ModelProcessor {
    private long numberOfInputs;
    private long numberOfOutputs;
    private List<LayerInfo> layers;
    private int width;
    private int height;

    @AllArgsConstructor
    private static class LayerInfo {
        long in;
        long out;
    }

    private ModelProcessor() {
        layers = new ArrayList<>();
    }

    static ModelProcessor process(File file, String sessionId) {
        try {
            return processWithExceptions(file, sessionId);
        } catch (UnsupportedKerasConfigurationException | IOException | InvalidKerasConfigurationException e) {
            e.printStackTrace();
        }

        return new ModelProcessor();
    }

    private static ModelProcessor processWithExceptions(File file, String sessionId) throws UnsupportedKerasConfigurationException, IOException, InvalidKerasConfigurationException {
        // TODO COPY FILE TO TEMP/...
        // TODO Unzip it later

        Unzipper unzipper = new Unzipper(file.toPath().toString(), "modelsdir/" + sessionId + "/");
        UnzippedModelFiles modelFiles = unzipper.extractFiles();

        File h5 = modelFiles.getH5();
        File config = modelFiles.getConfig();
        File sizeConfig = modelFiles.getSizeConfig();

        log.info("Reading H5: " + h5 + " and config: " + config);

        ModelProcessor mp = new ModelProcessor();

        MultiLayerNetwork model = KerasModelImport.importKerasSequentialModelAndWeights(config.getPath(), h5.getPath());

        int i = 0;
        int numberOfLayers = model.getLayers().length;

        for(Layer layer : model.getLayers()) {
            if(layer instanceof BaseLayer<?>) {
                BaseLayer<?> bLayer = (BaseLayer<?>)layer;

                long in = ((FeedForwardLayer) bLayer.getConf().getLayer()).getNIn();
                long out = ((FeedForwardLayer) bLayer.getConf().getLayer()).getNOut();

                if(i == 0) {
                    mp.numberOfInputs = in;
                }
                else if(i == numberOfLayers-1) {
                    mp.numberOfOutputs = out;
                }

                mp.layers.add(new LayerInfo(in, out));
            }

            ++i;
        }

        Pair<Integer, Integer> modelSize = readModelFile(sizeConfig);
        mp.width = modelSize.getFirst();
        mp.height = modelSize.getSecond();

        log.info(String.format("Processed model: in(%d) out(%d) layers(%d)", mp.numberOfInputs, mp.numberOfOutputs, mp.layers.size()));

        return mp;
    }

    private static Pair<Integer, Integer> readModelFile(File file) {
        try {
            if(file != null && file.exists()) {
                byte[] encoded = Files.readAllBytes(file.toPath());
                String json = new String(encoded, Charset.forName("UTF-8"));
                ObjectMapper objectMapper = new ObjectMapper();
                ModelSize size = objectMapper.readValue(json, ModelSize.class);

                return Pair.of(size.getWidth(), size.getHeight());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Pair.of(ProcessedImage.DEFAULT_WIDTH, ProcessedImage.DEFAULT_HEIGHT);
    }

    static List<Float> getOutputs(float pixels[], long modelId, String sessionId) throws Exception {
        Unzipper unzipper = new Unzipper("modelsdir/" + modelId + ".zip", "modelsdir/" + sessionId + "/");
        UnzippedModelFiles modelFiles = unzipper.extractFiles();

        File h5 = modelFiles.getH5();
        File config = modelFiles.getConfig();
        File sizeConfig = modelFiles.getSizeConfig();

        log.info("Reading H5: " + h5 + " and config: " + config);

        MultiLayerNetwork model = KerasModelImport.importKerasSequentialModelAndWeights(config.getPath(), h5.getPath());

        Pair<Integer, Integer> size = readModelFile(sizeConfig);

        INDArray input = Nd4j.create(1, 1, size.getFirst(), size.getSecond());
        int k = 0;
        for(int i = 0; i < size.getFirst(); ++i) {
            for(int j = 0; j < size.getSecond(); ++j) {
                input.putScalar(new int[] {0,0,i,j}, pixels[k++]);
            }
        }

        List<Float> outputs = new ArrayList<>();
        INDArray output = model.output(input);
        float max = output.getFloat(0);
        int indexMax = 0;
        for(int i = 0 ; i < output.length(); ++i) {
            outputs.add(output.getFloat(i));
            if(output.getFloat(i) > max) {
                max = output.getFloat(i);
                indexMax = i;
            }
        }

        System.out.println("Odp: " + indexMax);

        return outputs;
    }

}
