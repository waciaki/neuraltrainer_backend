package pl.edu.wat.security;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="users")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;

    String username;
    String password;
    String email;

    Boolean isAdmin;

    Date createdDate;
}
